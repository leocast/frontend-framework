export function MakeRequest(element, file){
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4) {
           
            if (this.status == 200) 
                element.innerHTML = this.responseText
            
            if (this.status == 404)
                element.innerHTML = `<center><h1>Element <b>${file}</b> don't found</h1></center>`
        }
    }
    xhttp.open("GET", file, true);
    xhttp.send();
}