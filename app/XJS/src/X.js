const Tools = require("./Tools")
const Engine = require("./Engine")

module.exports = 

class X {
    
    constructor(tagName, json, render = true){
        this.id = `tag${tagName}`
        this.tagName = tagName
        this.content = ""
        this.tag = document.getElementsByTagName(tagName)[0]
        this.attributes = this.tag.attributes
        this.html = this.tag.innerHTML
        this.div = this.SetDiv()
        this.data = json

        if(render)
            this.Render()
    }

    Update(json, render = true){
        this.data = json

        if(render)
            this.Render()
    }

    Render(){
      
        if(this.tag.hasAttribute('repeater'))
        {
            this.Repeater(this.html)
            this.div.innerHTML = this.content
        } else {
            this.content = Engine.FormatHtml(this.html, this.data)
            this.div.innerHTML = this.content
        }

        // this.div.style.display = 'flex'
    }

    SetDiv(){
        this.tag.innerHTML = `<div id='${this.id}'>${this.html}</div>`
        return document.getElementById(this.id)
    }

    Repeater(html){

        this.data.forEach((row, i) => {
           
            const content = Engine.FormatHtml(html, row)
            
            this.content += content
        }, this)

    }

    Clean(){
        this.content = ""
        this.div.innerHTML = this.content
    }
    
    IncludeHTML() { //Pensar en incluirlo más adelante
    
        const z = document.getElementsByTagName("include");
    
        for (let i = 0; i < z.length; i++) {
          const element = z[i];
          const file = element.getAttribute('location');
          if (file) {
            Tools.MakeRequest(element, file)
          }
        }
    }

}