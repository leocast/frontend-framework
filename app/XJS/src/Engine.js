export function FormatHtml(html, data){

    let newHtml = ""
    for(let property in data){

        const nuevoString = html.split('{')
        let rawElement = ""
        nuevoString.forEach(element => {
            
            if(element.includes('}'))
            {
                rawElement = element.split('}')[0]

                if (rawElement.includes(property)) 
                    newHtml = GetValues(html, rawElement, property, data, newHtml)
            }
        })
    }

    return newHtml

}

export function GetValues(html, rawElement, property, data, newHtml){
    
    if (rawElement.includes('.')) {
        
        const newRaw = rawElement.split('.')
        const trueRaw = newRaw[newRaw.length -1]

        const dataStr = JSON.stringify(data)
        
        const ident1 = rawElement.split('.')[0]
        const ident2 = rawElement.split('.')[1]
        
        const value = dataStr.split(`"${ident1}":`)[1].split(`"${trueRaw}":"`)[1].split('"')[0]
        
        if (ident1 == property && rawElement.includes(ident2)) {
            
            if (newHtml != "") 
                newHtml = newHtml.replace(`{${rawElement}}`, value)    
            else 
                newHtml = html.replace(`{${rawElement}}`, value)
        }
      
    } else {
        if (newHtml != "") 
            newHtml = newHtml.replace(`{${rawElement}}`, data[rawElement])    
        else 
            newHtml = html.replace(`{${rawElement}}`, data[rawElement])
    }

    return newHtml
}