var X =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./app/XJS/src/X.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./app/XJS/src/Engine.js":
/*!*******************************!*\
  !*** ./app/XJS/src/Engine.js ***!
  \*******************************/
/*! exports provided: FormatHtml, GetValues */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"FormatHtml\", function() { return FormatHtml; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"GetValues\", function() { return GetValues; });\nfunction FormatHtml(html, data){\r\n\r\n    let newHtml = \"\"\r\n    for(let property in data){\r\n\r\n        const nuevoString = html.split('{')\r\n        let rawElement = \"\"\r\n        nuevoString.forEach(element => {\r\n            \r\n            if(element.includes('}'))\r\n            {\r\n                rawElement = element.split('}')[0]\r\n\r\n                if (rawElement.includes(property)) \r\n                    newHtml = GetValues(html, rawElement, property, data, newHtml)\r\n            }\r\n        })\r\n    }\r\n\r\n    return newHtml\r\n\r\n}\r\n\r\nfunction GetValues(html, rawElement, property, data, newHtml){\r\n    \r\n    if (rawElement.includes('.')) {\r\n        \r\n        const newRaw = rawElement.split('.')\r\n        const trueRaw = newRaw[newRaw.length -1]\r\n\r\n        const dataStr = JSON.stringify(data)\r\n        \r\n        const ident1 = rawElement.split('.')[0]\r\n        const ident2 = rawElement.split('.')[1]\r\n        \r\n        const value = dataStr.split(`\"${ident1}\":`)[1].split(`\"${trueRaw}\":\"`)[1].split('\"')[0]\r\n        \r\n        if (ident1 == property && rawElement.includes(ident2)) {\r\n            \r\n            if (newHtml != \"\") \r\n                newHtml = newHtml.replace(`{${rawElement}}`, value)    \r\n            else \r\n                newHtml = html.replace(`{${rawElement}}`, value)\r\n        }\r\n      \r\n    } else {\r\n        if (newHtml != \"\") \r\n            newHtml = newHtml.replace(`{${rawElement}}`, data[rawElement])    \r\n        else \r\n            newHtml = html.replace(`{${rawElement}}`, data[rawElement])\r\n    }\r\n\r\n    return newHtml\r\n}//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9hcHAvWEpTL3NyYy9FbmdpbmUuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9YLy4vYXBwL1hKUy9zcmMvRW5naW5lLmpzPzhiMTIiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGZ1bmN0aW9uIEZvcm1hdEh0bWwoaHRtbCwgZGF0YSl7XHJcblxyXG4gICAgbGV0IG5ld0h0bWwgPSBcIlwiXHJcbiAgICBmb3IobGV0IHByb3BlcnR5IGluIGRhdGEpe1xyXG5cclxuICAgICAgICBjb25zdCBudWV2b1N0cmluZyA9IGh0bWwuc3BsaXQoJ3snKVxyXG4gICAgICAgIGxldCByYXdFbGVtZW50ID0gXCJcIlxyXG4gICAgICAgIG51ZXZvU3RyaW5nLmZvckVhY2goZWxlbWVudCA9PiB7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBpZihlbGVtZW50LmluY2x1ZGVzKCd9JykpXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHJhd0VsZW1lbnQgPSBlbGVtZW50LnNwbGl0KCd9JylbMF1cclxuXHJcbiAgICAgICAgICAgICAgICBpZiAocmF3RWxlbWVudC5pbmNsdWRlcyhwcm9wZXJ0eSkpIFxyXG4gICAgICAgICAgICAgICAgICAgIG5ld0h0bWwgPSBHZXRWYWx1ZXMoaHRtbCwgcmF3RWxlbWVudCwgcHJvcGVydHksIGRhdGEsIG5ld0h0bWwpXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KVxyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBuZXdIdG1sXHJcblxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gR2V0VmFsdWVzKGh0bWwsIHJhd0VsZW1lbnQsIHByb3BlcnR5LCBkYXRhLCBuZXdIdG1sKXtcclxuICAgIFxyXG4gICAgaWYgKHJhd0VsZW1lbnQuaW5jbHVkZXMoJy4nKSkge1xyXG4gICAgICAgIFxyXG4gICAgICAgIGNvbnN0IG5ld1JhdyA9IHJhd0VsZW1lbnQuc3BsaXQoJy4nKVxyXG4gICAgICAgIGNvbnN0IHRydWVSYXcgPSBuZXdSYXdbbmV3UmF3Lmxlbmd0aCAtMV1cclxuXHJcbiAgICAgICAgY29uc3QgZGF0YVN0ciA9IEpTT04uc3RyaW5naWZ5KGRhdGEpXHJcbiAgICAgICAgXHJcbiAgICAgICAgY29uc3QgaWRlbnQxID0gcmF3RWxlbWVudC5zcGxpdCgnLicpWzBdXHJcbiAgICAgICAgY29uc3QgaWRlbnQyID0gcmF3RWxlbWVudC5zcGxpdCgnLicpWzFdXHJcbiAgICAgICAgXHJcbiAgICAgICAgY29uc3QgdmFsdWUgPSBkYXRhU3RyLnNwbGl0KGBcIiR7aWRlbnQxfVwiOmApWzFdLnNwbGl0KGBcIiR7dHJ1ZVJhd31cIjpcImApWzFdLnNwbGl0KCdcIicpWzBdXHJcbiAgICAgICAgXHJcbiAgICAgICAgaWYgKGlkZW50MSA9PSBwcm9wZXJ0eSAmJiByYXdFbGVtZW50LmluY2x1ZGVzKGlkZW50MikpIHtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGlmIChuZXdIdG1sICE9IFwiXCIpIFxyXG4gICAgICAgICAgICAgICAgbmV3SHRtbCA9IG5ld0h0bWwucmVwbGFjZShgeyR7cmF3RWxlbWVudH19YCwgdmFsdWUpICAgIFxyXG4gICAgICAgICAgICBlbHNlIFxyXG4gICAgICAgICAgICAgICAgbmV3SHRtbCA9IGh0bWwucmVwbGFjZShgeyR7cmF3RWxlbWVudH19YCwgdmFsdWUpXHJcbiAgICAgICAgfVxyXG4gICAgICBcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaWYgKG5ld0h0bWwgIT0gXCJcIikgXHJcbiAgICAgICAgICAgIG5ld0h0bWwgPSBuZXdIdG1sLnJlcGxhY2UoYHske3Jhd0VsZW1lbnR9fWAsIGRhdGFbcmF3RWxlbWVudF0pICAgIFxyXG4gICAgICAgIGVsc2UgXHJcbiAgICAgICAgICAgIG5ld0h0bWwgPSBodG1sLnJlcGxhY2UoYHske3Jhd0VsZW1lbnR9fWAsIGRhdGFbcmF3RWxlbWVudF0pXHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIG5ld0h0bWxcclxufSJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./app/XJS/src/Engine.js\n");

/***/ }),

/***/ "./app/XJS/src/Tools.js":
/*!******************************!*\
  !*** ./app/XJS/src/Tools.js ***!
  \******************************/
/*! exports provided: MakeRequest */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"MakeRequest\", function() { return MakeRequest; });\nfunction MakeRequest(element, file){\r\n    const xhttp = new XMLHttpRequest();\r\n    xhttp.onreadystatechange = function() {\r\n        if (this.readyState == 4) {\r\n           \r\n            if (this.status == 200) \r\n                element.innerHTML = this.responseText\r\n            \r\n            if (this.status == 404)\r\n                element.innerHTML = `<center><h1>Element <b>${file}</b> don't found</h1></center>`\r\n        }\r\n    }\r\n    xhttp.open(\"GET\", file, true);\r\n    xhttp.send();\r\n}//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9hcHAvWEpTL3NyYy9Ub29scy5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovL1gvLi9hcHAvWEpTL3NyYy9Ub29scy5qcz8xZGUwIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBmdW5jdGlvbiBNYWtlUmVxdWVzdChlbGVtZW50LCBmaWxlKXtcclxuICAgIGNvbnN0IHhodHRwID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XHJcbiAgICB4aHR0cC5vbnJlYWR5c3RhdGVjaGFuZ2UgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAodGhpcy5yZWFkeVN0YXRlID09IDQpIHtcclxuICAgICAgICAgICBcclxuICAgICAgICAgICAgaWYgKHRoaXMuc3RhdHVzID09IDIwMCkgXHJcbiAgICAgICAgICAgICAgICBlbGVtZW50LmlubmVySFRNTCA9IHRoaXMucmVzcG9uc2VUZXh0XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBpZiAodGhpcy5zdGF0dXMgPT0gNDA0KVxyXG4gICAgICAgICAgICAgICAgZWxlbWVudC5pbm5lckhUTUwgPSBgPGNlbnRlcj48aDE+RWxlbWVudCA8Yj4ke2ZpbGV9PC9iPiBkb24ndCBmb3VuZDwvaDE+PC9jZW50ZXI+YFxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIHhodHRwLm9wZW4oXCJHRVRcIiwgZmlsZSwgdHJ1ZSk7XHJcbiAgICB4aHR0cC5zZW5kKCk7XHJcbn0iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./app/XJS/src/Tools.js\n");

/***/ }),

/***/ "./app/XJS/src/X.js":
/*!**************************!*\
  !*** ./app/XJS/src/X.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("const Tools = __webpack_require__(/*! ./Tools */ \"./app/XJS/src/Tools.js\")\r\nconst Engine = __webpack_require__(/*! ./Engine */ \"./app/XJS/src/Engine.js\")\r\n\r\nmodule.exports = \r\n\r\nclass X {\r\n    \r\n    constructor(tagName, json, render = true){\r\n        this.id = `tag${tagName}`\r\n        this.tagName = tagName\r\n        this.content = \"\"\r\n        this.tag = document.getElementsByTagName(tagName)[0]\r\n        this.attributes = this.tag.attributes\r\n        this.html = this.tag.innerHTML\r\n        this.div = this.SetDiv()\r\n        this.data = json\r\n\r\n        if(render)\r\n            this.Render()\r\n    }\r\n\r\n    Update(json, render = true){\r\n        this.data = json\r\n\r\n        if(render)\r\n            this.Render()\r\n    }\r\n\r\n    Render(){\r\n      \r\n        if(this.tag.hasAttribute('repeater'))\r\n        {\r\n            this.Repeater(this.html)\r\n            this.div.innerHTML = this.content\r\n        } else {\r\n            this.content = Engine.FormatHtml(this.html, this.data)\r\n            this.div.innerHTML = this.content\r\n        }\r\n\r\n        // this.div.style.display = 'flex'\r\n    }\r\n\r\n    SetDiv(){\r\n        this.tag.innerHTML = `<div id='${this.id}'>${this.html}</div>`\r\n        return document.getElementById(this.id)\r\n    }\r\n\r\n    Repeater(html){\r\n\r\n        this.data.forEach((row, i) => {\r\n           \r\n            const content = Engine.FormatHtml(html, row)\r\n            \r\n            this.content += content\r\n        }, this)\r\n\r\n    }\r\n\r\n    Clean(){\r\n        this.content = \"\"\r\n        this.div.innerHTML = this.content\r\n    }\r\n    \r\n    IncludeHTML() { //Pensar en incluirlo más adelante\r\n    \r\n        const z = document.getElementsByTagName(\"include\");\r\n    \r\n        for (let i = 0; i < z.length; i++) {\r\n          const element = z[i];\r\n          const file = element.getAttribute('location');\r\n          if (file) {\r\n            Tools.MakeRequest(element, file)\r\n          }\r\n        }\r\n    }\r\n\r\n}//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9hcHAvWEpTL3NyYy9YLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vWC8uL2FwcC9YSlMvc3JjL1guanM/M2Y5YiJdLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCBUb29scyA9IHJlcXVpcmUoXCIuL1Rvb2xzXCIpXHJcbmNvbnN0IEVuZ2luZSA9IHJlcXVpcmUoXCIuL0VuZ2luZVwiKVxyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBcclxuXHJcbmNsYXNzIFgge1xyXG4gICAgXHJcbiAgICBjb25zdHJ1Y3Rvcih0YWdOYW1lLCBqc29uLCByZW5kZXIgPSB0cnVlKXtcclxuICAgICAgICB0aGlzLmlkID0gYHRhZyR7dGFnTmFtZX1gXHJcbiAgICAgICAgdGhpcy50YWdOYW1lID0gdGFnTmFtZVxyXG4gICAgICAgIHRoaXMuY29udGVudCA9IFwiXCJcclxuICAgICAgICB0aGlzLnRhZyA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKHRhZ05hbWUpWzBdXHJcbiAgICAgICAgdGhpcy5hdHRyaWJ1dGVzID0gdGhpcy50YWcuYXR0cmlidXRlc1xyXG4gICAgICAgIHRoaXMuaHRtbCA9IHRoaXMudGFnLmlubmVySFRNTFxyXG4gICAgICAgIHRoaXMuZGl2ID0gdGhpcy5TZXREaXYoKVxyXG4gICAgICAgIHRoaXMuZGF0YSA9IGpzb25cclxuXHJcbiAgICAgICAgaWYocmVuZGVyKVxyXG4gICAgICAgICAgICB0aGlzLlJlbmRlcigpXHJcbiAgICB9XHJcblxyXG4gICAgVXBkYXRlKGpzb24sIHJlbmRlciA9IHRydWUpe1xyXG4gICAgICAgIHRoaXMuZGF0YSA9IGpzb25cclxuXHJcbiAgICAgICAgaWYocmVuZGVyKVxyXG4gICAgICAgICAgICB0aGlzLlJlbmRlcigpXHJcbiAgICB9XHJcblxyXG4gICAgUmVuZGVyKCl7XHJcbiAgICAgIFxyXG4gICAgICAgIGlmKHRoaXMudGFnLmhhc0F0dHJpYnV0ZSgncmVwZWF0ZXInKSlcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRoaXMuUmVwZWF0ZXIodGhpcy5odG1sKVxyXG4gICAgICAgICAgICB0aGlzLmRpdi5pbm5lckhUTUwgPSB0aGlzLmNvbnRlbnRcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRlbnQgPSBFbmdpbmUuRm9ybWF0SHRtbCh0aGlzLmh0bWwsIHRoaXMuZGF0YSlcclxuICAgICAgICAgICAgdGhpcy5kaXYuaW5uZXJIVE1MID0gdGhpcy5jb250ZW50XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyB0aGlzLmRpdi5zdHlsZS5kaXNwbGF5ID0gJ2ZsZXgnXHJcbiAgICB9XHJcblxyXG4gICAgU2V0RGl2KCl7XHJcbiAgICAgICAgdGhpcy50YWcuaW5uZXJIVE1MID0gYDxkaXYgaWQ9JyR7dGhpcy5pZH0nPiR7dGhpcy5odG1sfTwvZGl2PmBcclxuICAgICAgICByZXR1cm4gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQodGhpcy5pZClcclxuICAgIH1cclxuXHJcbiAgICBSZXBlYXRlcihodG1sKXtcclxuXHJcbiAgICAgICAgdGhpcy5kYXRhLmZvckVhY2goKHJvdywgaSkgPT4ge1xyXG4gICAgICAgICAgIFxyXG4gICAgICAgICAgICBjb25zdCBjb250ZW50ID0gRW5naW5lLkZvcm1hdEh0bWwoaHRtbCwgcm93KVxyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgdGhpcy5jb250ZW50ICs9IGNvbnRlbnRcclxuICAgICAgICB9LCB0aGlzKVxyXG5cclxuICAgIH1cclxuXHJcbiAgICBDbGVhbigpe1xyXG4gICAgICAgIHRoaXMuY29udGVudCA9IFwiXCJcclxuICAgICAgICB0aGlzLmRpdi5pbm5lckhUTUwgPSB0aGlzLmNvbnRlbnRcclxuICAgIH1cclxuICAgIFxyXG4gICAgSW5jbHVkZUhUTUwoKSB7IC8vUGVuc2FyIGVuIGluY2x1aXJsbyBtw6FzIGFkZWxhbnRlXHJcbiAgICBcclxuICAgICAgICBjb25zdCB6ID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJpbmNsdWRlXCIpO1xyXG4gICAgXHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB6Lmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICBjb25zdCBlbGVtZW50ID0geltpXTtcclxuICAgICAgICAgIGNvbnN0IGZpbGUgPSBlbGVtZW50LmdldEF0dHJpYnV0ZSgnbG9jYXRpb24nKTtcclxuICAgICAgICAgIGlmIChmaWxlKSB7XHJcbiAgICAgICAgICAgIFRvb2xzLk1ha2VSZXF1ZXN0KGVsZW1lbnQsIGZpbGUpXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufSJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./app/XJS/src/X.js\n");

/***/ })

/******/ });